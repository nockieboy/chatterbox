# Chatterbox

This is a basic project generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.3 and [Angular 4](https://angular.io/). It's primary purpose is to help me learn more about Angular, whilst providing real-time messaging for key people.

## Installation

Download and install the latest version of [NodeJS](https://nodejs.org/en/download/) if you don't already have it.
Run `npm install` in the project folder once you've cloned the repo.
If you don't already have it, `npm install -g @angular/cli` will install the latest Angular CLI which is (obviously) required.
Finally, `ng serve -o` to open a browser and run the app!

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Your own chat database

The messaging database for chatterbox relies on [Firebase](https://firebase.google.com/), a rather handy set of tools provided by Google. Follow their [guide](https://firebase.google.com/docs/web/setup?authuser=0) on setting up an app and connecting to your own Firebase Realtime Database.

## Further help

Contact the developer for more information.  You can use the app to do just that! [chatterbox](https://chatterbox-10103.firebaseapp.com).
