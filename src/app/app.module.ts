import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2EmojiModule } from 'ng2-emoji';

import { LinkifyPipe } from './linkify.pipe';

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDQv8n_tQ7plWN28VGzxKRJ0aCWfZ9WgUI",
    authDomain: "chatterbox-10103.firebaseapp.com",
    databaseURL: "https://chatterbox-10103.firebaseio.com",
    projectId: "chatterbox-10103",
    storageBucket: "chatterbox-10103.appspot.com"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    LinkifyPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    Ng2EmojiModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
