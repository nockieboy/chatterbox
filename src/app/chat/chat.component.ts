
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PipeTransform, Pipe } from '@angular/core';
import * as firebase from 'firebase/app';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  host: {'window:beforeunload':'logout'},
  animations: [
    trigger('newMsgAnimation', [
      state('hidden', style({
        width: '30px',
      })),
      state('shown', style({
        width: '300px',
      })),
      transition('hidden => shown', animate('300ms ease-out')),
      transition('shown => hidden', animate('300ms ease-out')),
    ]),
  ]
}) 

/*
 * This component handles the chat window and message display,
 * along with login, logout and notification of new messages.
 * This could be split into a control and a couple of services
 * to handle user authentication/login and the chat notifications.
 */
export class ChatComponent implements OnInit {

  title = document.title;
  state: string = 'hidden';   // Animation starting property
  count = 0;
  msgClass = "inner_txt";
  message: string = '';
  objectKeys = Object.keys;
  messages: FirebaseListObservable<any[]>;
  name: any;
  user: Observable<firebase.User>;
  current_user: firebase.User;
  last_action = 0;
  uid = '';
  mute_toast = false;
  user_authenticated = false;
  logoutTimer = 3600000; // 3600000 = 1 hour timeout

  constructor(public afAuth: AngularFireAuth, public db: AngularFireDatabase) {
    this.user = this.afAuth.authState;
    /*
     * Set up user authentication and once completed set up a few variables
     * and try to get the user data from the database.
     */
    this.afAuth.idToken.subscribe(
      (auth) => {
        if (auth == null) {
          this.uid = '';
        } else {
          this.uid = this.afAuth.auth.currentUser.uid;
          this.current_user = this.afAuth.auth.currentUser;
          this.user_authenticated = true;
          console.log("User " + this.current_user.displayName + " authenticated.");
          this.getUserData(false);
        }
      }
    );
    /*
     * Set up event for changes in the chat_messages database.
     * Any changes to the chat_messages array in the Firebase database
     * will trigger an event via this listener, which will call the
     * toast() function and play a notification sound under certain
     * circumstances (if the user hasn't muted notifications and if
     * the most recent message isn't from the user theirself.)
     */
    firebase.database().ref('chat_messages').on('value', snapshot => {
        this.toast();
    })
  }

  /*
   * Currently user login/logout and user authentication (albeit via popup)
   * are all handled from this component.
   */
  login() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then(success=>{
      this.uid = this.afAuth.auth.currentUser.uid;
      this.getUserData(true);
      this.setUserLoggedIn(this.afAuth.auth.currentUser.uid);
      var that = this;
      setTimeout(function() {
        that.message = that.afAuth.auth.currentUser.displayName + ' entered the chat.';
        that.newMessage();  
      }, 1000)
    })
    .catch(function (error) {
      console.log(error.message);
      console.log("Log in failure!");
    });
  }

  logout() {
    this.message = this.afAuth.auth.currentUser.displayName + ' left the chat.';
    this.newMessage();
    console.log("User logged out.");
    this.setUserData(false);
    this.afAuth.auth.signOut();
    this.uid = '';
    this.current_user = null;
    this.user_authenticated = false;
  }

  animateMe() {
    this.state = (this.state === 'hidden' ? 'shown' : 'hidden');
  }

  // Actions on app initialisation
  ngOnInit() {
    /*
     * Call getUserData and getChatData after a
     * 1 second pause to give Firebase time to
     * authenticate the current user asynchronously
     */
    var that = this;
    setTimeout(function () {
      //that.getUserData()
      that.getChatData()
      that.checkUserLoginStatuses()
    }, 1500);
    setInterval(function() {
      // Check user logins every minute for inactivity
      that.checkUserLoginStatuses()
    }, 60000);
  }

  /*
   * Get the messages in the Firebase db
   */
  getChatData() {
    if (!this.user_authenticated) {
      return;
    }
    this.messages = this.db.list('chat_messages', {
      query: {
        limitToLast: 20
      }
    });
  }

  getUIDFromLastMessage (callback) {
    if (!this.user_authenticated) {
      return;
    }
    firebase.database().ref('/chat_messages/').limitToLast(1).once("child_added", function (snapshot) {
      callback(snapshot.val());
    });
  }

  /*
   * This function checks through ALL users in the database
   * to see how long they've been inactive for and logs them
   * out if they've been inactive for longer than logoutTimer.
   */
  checkUserLoginStatuses() {
    console.log("checkUserLoginStatuses() called.");
    if (this.user_authenticated) {
      var that = this;
      this.db.list('/users/').subscribe(items => {
        items.forEach(item => {
          var activityTimer = Date.now() - item.lastActive;
          if (activityTimer > this.logoutTimer) {
            // User has been inactive for too long - show them logged out
            if (item.loggedIn) {
              if (item.uid === this.current_user.uid) {
                // Current user is inactive - log them out
                this.logout();
              } else {
                // Another user is inactive - log them out
                this.setUserLoggedOut(item.uid);
              }
            }
          }
        })
      });
    } else {
      console.log("Unable to check User login statuses as current user isn't logged in.");
    }
  }

  /*
   * Get the user's meta data
   */
  getUserData(silent) {
    if (!this.user_authenticated) {
      return;
    }
    var that = this;
    this.current_user = this.afAuth.auth.currentUser;
    var userId = this.current_user.uid;

    firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
      var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      that.mute_toast = (snapshot.val() && snapshot.val().mute) || false;
      that.last_action = (snapshot.val() && snapshot.val().lastAction) || 0;
      that.uid = (snapshot.val() && snapshot.val().uid) || '';
      console.log("User data retrieved...");
      //if (!silent) {
        that.getChatData();
      //}
    });
  }

  /*
   * Save the user's meta data
   */
  setUserData(logged) {
    if (!this.user_authenticated) {
      return;
    }
    this.db.database.ref('users/' + this.current_user.uid).set({
      username: this.current_user.displayName,
      email: this.current_user.email,
      image: this.current_user.photoURL,
      mute: this.mute_toast,
      uid: this.current_user.uid,
      lastActive: Date.now(),
      loggedIn: logged
    });
  }

  /*
   * Set user record with UID userID's loggedIn value to true
   */
  setUserLoggedIn(userID) {
    if (!this.user_authenticated) {
      return;
    }
    this.db.database.ref('users/' + userID).update({
      loggedIn: true
    });
  }

  /*
   * Set user record with UID userID's loggedIn value to false
   */
  setUserLoggedOut(userID) {
    if (!this.user_authenticated) {
      return;
    }
    this.db.database.ref('users/' + userID).update({
      loggedIn: false
    });
    console.log("UID: " + userID + " logged out.")
  }

  /*
   * Toggle the mute_toast boolean to turn on/off
   * the notification bell noise.  Also updates the
   * user database with the updated preference.
   */
  muteToggle() {
    if (!this.user_authenticated) {
      return;
    }
    this.mute_toast = !this.mute_toast;
    var user = firebase.auth().currentUser;
    this.db.database.ref('users/' + user.uid).update({
      mute: this.mute_toast
    });
    this.onFocus();
  }

  /*
   * newMessage takes the HTMLInputElement so that the function
   * can access the input text as well as the input element itself.
   */
  newMessage() {
    if (this.afAuth.auth.currentUser != null) {
      this.onFocus();
      if (this.message === '') {
        // Skip empty messages
        return;
      }
      var msg_to_send = {
        message: this.message,
        owner: this.afAuth.auth.currentUser.displayName,
        email: this.afAuth.auth.currentUser.email,
        uid: this.afAuth.auth.currentUser.uid,
        image: this.afAuth.auth.currentUser.photoURL,
        timestamp: Date.now()
      };
      // Push the message up to the Firebase db
      if (this.messages) {
        this.messages.push(msg_to_send);
      } else {
        console.log("Unable to push message - this.messages is undefined.");
      }
      // Clear the contents of the input box
      this.message = '';
    }
  }

  /*
   * Called every time there is a new message
   * (This includes user-created messages)
   */
  toast() {
    if (!this.user_authenticated) {
      return;
    }
    this.count++;
    var newTitle = '(' + this.count + ') ' + this.title;
    document.title = newTitle;

    var that = this;
    this.getUIDFromLastMessage(function (msg) {
      // Check the UID of the poster of the last message
      if (msg.uid == that.afAuth.auth.currentUser.uid) {
        // UID matches current user - do not play any notification sounds
        return;
      } else {
        // UID doesn't match - check preferences before playing sound
        if (!that.mute_toast) {
          var audio = new Audio();
          audio.src = "/assets/dingaling.mp3";
          audio.load();
          audio.play();
        }
      }
    });
    
  }

  /*
   * Update the current user's lastActive timestamp
   * to show activity.
   */
  logTick() {
    if (this.current_user) {
      this.db.database.ref('users/' + this.current_user.uid).update({
        lastActive: Date.now()
      });
    }
  }

  // This is called whenever the message input gets focus.
  onFocus() {
    if (!this.user_authenticated) {
      return;
    }
    this.count = 0;
    document.title = 'Chatterbox';
    this.logTick();
  }

}
